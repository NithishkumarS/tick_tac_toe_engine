/*
 * interface.h
 * @brief Interface between the game class and visualizers,
 * 		  player class and user input
 *  Created on: Aug 24, 2020
 *      Author: nithish
 */

#ifndef INCLUDE_INTERFACE_H_
#define INCLUDE_INTERFACE_H_

#include <iostream>

// Singleton Design

class Interface {
	int grid_size;
	static Interface interfaceInstance;
	std::string **state;
public:
	/*
	 *  Function to return the instance of the class
	 */
	static Interface* getInstance() {
		return &interfaceInstance;
	}

	/*
	 *  Receives the next move from the player class modifies the state
	 */
	virtual std::pair<int, int> returnPlayerInput();

	/*
	 *  Displays Instructions
	 */
	virtual void InitialDisplay();

	/*
	 *  Function to view the grid after each move / visualizer
	 */
	virtual void Display(int**, std::string, std::string);
	Interface() {
		grid_size = 3;
		state = new std::string*[grid_size];
		for (int i = 0; i < grid_size; i++) {
			state[i] = new std::string[grid_size];
			for (int j = 0; j < grid_size; j++)
				state[i][j] = " ";
		}

	}

	/*
	 * Destructor
	 */
	virtual ~Interface();
};

#endif /* INCLUDE_INTERFACE_H_ */

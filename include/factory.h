/*
 * factory.h
 * @brief Helps choose the type of player using factory pattern design
 *  Created on: Aug 24, 2020
 *      Author: nithish
 */

#ifndef INCLUDE_FACTORY_H_
#define INCLUDE_FACTORY_H_

#include "player.h"
#include "humanAgent.h"
#include "AI.h"

player* player::Agents(std::string player_type) {

	if (player_type.compare("human") == 0) {
		return new human;
	} else if (player_type.compare("AI") == 0) {
		return new AI;
	} else
		throw;
	// AI agent could be added
}

#endif /* INCLUDE_FACTORY_H_ */

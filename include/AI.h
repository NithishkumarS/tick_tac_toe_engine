/*
 * AI.h
 *
 *  Created on: Aug 24, 2020
 *      Author: nithish
 */

#ifndef INCLUDE_AI_H_
#define INCLUDE_AI_H_

#include"player.h"

class AI: public player {
	int **state;
public:
	/*
	 *  Constructor
	 */
	AI();

	/*
	 *	Destructor
	 */
	virtual ~AI();

	/*
	 *  @brief function returns the first free position
	 *  param  state of the grid
	 *  return (X,Y) coordinates
	 */
	std::pair<int, int> move(int**);

};

#endif /* INCLUDE_AI_H_ */

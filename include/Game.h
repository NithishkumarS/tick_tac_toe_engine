#pragma once
/*
 * Game.h
 * @brief Class that controls the flow of the game and agents
 *
 *  Created on: Aug 23, 2020
 *      Author: nithish
 */

#ifndef INCLUDE_GAME_H_
#define INCLUDE_GAME_H_

#include "state.h"
#include "interface.h"
#include "player.h"
#include <string>

class Game_Engine {
	std::string game_type;
	int size;
	GameState board;
	player *agent1, *agent2;
	std::string token1, token2;
	Interface *ifc_obj; // = Interface::getInstance();
public:
	/*
	 * Constructor
	 * param1 type of the game
	 * param2 token 1
	 * param2 token 2
	 * param3 size of grid
	 */
	Game_Engine(std::string, std::string token1 = "X", std::string token2 = "O",
			int _size = 3);

	/*
	 * Destructor
	 */
	~Game_Engine();

	/*
	 * bool to player mapping
	 */
	int getPlayer(bool);

	/*
	 * Function that runs the play
	 */
	void play();

};

#endif /* INCLUDE_GAME_H_ */

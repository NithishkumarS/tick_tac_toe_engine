/*
 * humanAgent.h
 *
 *  Created on: Aug 24, 2020
 *      Author: nithish
 */

#ifndef INCLUDE_HUMANAGENT_H_
#define INCLUDE_HUMANAGENT_H_

#include "player.h"

class human: public player {

	Interface *ifc_obj = Interface::getInstance();

public:
	/*
	 *  Constructor
	 */

	human();

	/*
	 *	Destructor
	 */
	virtual ~human();

	/*
	 *  @brief function returns the first free position
	 *  param  state of the grid
	 *  return (X,Y) coordinates
	 */
	std::pair<int, int> move(int**);

};

#endif /* INCLUDE_HUMANAGENT_H_ */

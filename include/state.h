/*
 * State.h
 *
 *  Created on: Aug 23, 2020
 *      Author: nithish
 */

#ifndef INCLUDE_STATE_H_
#define INCLUDE_STATE_H_

#include <iostream>
#include <vector>

class GameState {
	int **state;
	int size;
	int move_id;
public:
	/*
	 * Constructor
	 */
	GameState(int _size = 3);

	/*
	 * Initializes an empty game board
	 */
	void Initialize();

	/*
	 * brief Check if there is a winning configuration
	 * return Bool value from the check
	 */
	bool isGameOver();

	/*
	 * brief Checks if the slot is empty
	 * return Bool value from the check
	 */
	bool isValidMove(int x, int y);

	/*
	 * brief Modifies the state of the grid
	 * param1  row
	 * param2  column
	 * param3  player
	 */
	void Move(int x, int y, int player);

	/*
	 * brief Returns the state value
	 */
	int returnSlot(int x, int y);

	/*
	 * brief Function to access the state matrix
	 * return state
	 */
	int** returnState();

	/*
	 * Destructor
	 */
	~GameState();
};

#endif /* INCLUDE_STATE_H_ */

/*
 * player.h
 *
 *  Created on: Aug 24, 2020
 *      Author: nithish
 */

#ifndef INCLUDE_PLAYER_H_
#define INCLUDE_PLAYER_H_

#include <iostream>
#include "interface.h"

class player {
public:
	/*
	 * Constructor
	 */
	player() {
	}
	;

	/*
	 * Virtual Destructor
	 */
	virtual ~player() {
	}
	;

	/**
	 * Function to create an player object from the input
	 * params :  type of player
	 * return :  player object
	 */
	static player* Agents(std::string player_type);

	/*
	 * Pure virtual function to that has to redefined for the corresponding move
	 * params :  Current state of grid
	 * return :  Input location
	 */

	virtual std::pair<int, int> move(int**)=0;

};

#endif /* INCLUDE_PLAYER_H_ */

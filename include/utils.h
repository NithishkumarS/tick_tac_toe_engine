/*
 * utils.h
 *
 *  Created on: Aug 23, 2020
 *      Author: nithish
 */

#ifndef INCLUDE_UTILS_H_
#define INCLUDE_UTILS_H_

#include <iostream>
#include <vector>
#include <fstream>
#include <map>
#include <string>
#include <variant>

bool checkArguments(int argc, char* argv[]){
  if (argc < 7){
	  std::cout << "Missing arguments "<<std::endl;
    return false;
  }
  else{ // Use of wrong engine name
	  std::string game_type = argv[2];
    if (game_type != "single" and game_type != "multi" ){
    	std::cout << "Choose one from \"single\" or \"multi\"."<<std::endl;
      return false;
    }
  }
  return true;
}

std::map<std::string, std::string> parseInput(int argc, char* argv[]){

	std::map<std::string, std::string> parsed;
	parsed[argv[1]] = argv[2];
	std::string game_type = parsed[argv[1]];
	int i =3;
	while( i < argc){
		parsed[argv[i]] = argv[i+1];
		i +=2;
	}

	return parsed;
}


#endif /* INCLUDE_UTILS_H_ */

# Tic Tac Toe

This repository is my modular implementation of the Tic Tac Toe game using singleton and factory pattern design at appropriate scenarios. The goal was to develop a object oriented design which is suitable for future enhancements. <br/>

The default size of the grid is 3x3. But the size can be provided as an input in the command line. So can the token of choice be given as an input. <br/>

Interface class is capable of incorporating other GUI visulaizers. <br/>

![Class segregation](UML Class.png)
## Requirements
```
cmake >= 3.5
gcc -std=c++0x

```
Clone the repository :  git clone https://gitlab.com/NithishkumarS/tick_tac_toe_engine.git

## Build instructions
```
cd tick_tac_toe_engine/
mkdir build
cd build
cmake ..
make
```

## Clean the build
```
cd tick_tac_toe_engine/
rm -f build
```


## Instructions to run the engines with command line input
```
cd build
./src/TicTacToe game <GameType> token1 <Token1-Character> token2 <Token1-Character> size <size-grid>

./src/TicTacToe game single token1 X token2 O size 3
./src/TicTacToe game multi token1 X token2 O size 3

```
Token2 is for the second player, human or AI. 

## Instructions to run the unit tests 
Testing is done using the google test framework
```
cd build
./test/cpp-test
```


##Notes

Design <br/>
Game <br/>
board <br/>
  GameOver()	<br/>
player : Factory method<br/>
Interface : Singleton <br/>
	Allowing future enhancements <br/>
Main <br/>
	Option to choose single player or multiplayer <br/>
	Grid size input <br/>
	Tokens <br/>
	

```
MIT License

Copyright (c) 2020 Nithish Sanjeev Kumar

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```


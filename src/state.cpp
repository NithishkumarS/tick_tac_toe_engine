/*
 * state.cpp
 *
 *  Created on: Aug 23, 2020
 *      Author: nithish
 */

#include "state.h"
#include <vector>

using namespace std;

GameState::GameState(int _size) {
	size = _size;
	Initialize();
}

GameState::~GameState() {
	for (int i = 0; i < size; i++)
		delete[] state[i];
	delete[] state;
}

void GameState::Initialize() {

	state = new int*[size];
	for (int i = 0; i < size; i++) {
		state[i] = new int[size];
		for (int j = 0; j < size; j++)
			state[i][j] = 0;
	}
	move_id = 0;
}

bool GameState::isGameOver() {

	bool result;
	// rows
	for (int i = 0; i < size; i++) {
		result = true;
		for (int j = 0; j < size - 1; j++) {
			if (state[i][j] != state[i][j + 1] || state[i][j] == 0) {
				result &= false;
			}
		}
		if (result)
			return result;
	}

	// columns
	for (int j = 0; j < size; ++j) {
		result = true;
		for (int i = 0; i < size - 1; ++i) {
			if (state[i][j] != state[i + 1][j] || state[i][j] == 0) {
				result &= false;
			}
		}
		if (result)
			return result;
	}

	result = true;

	// Diagonal
	for (int j = 0; j < size - 1; j++) {
		if (state[j][j] != state[j + 1][j + 1] || state[j][j] == 0)
			result &= false;
	}

	if (result)
		return result;

	result = true;

	// Anti Diagonal
	for (int j = 0; j < size - 1; j++) {
		if (state[j][size - 1 - j] != state[j + 1][size - 2 - j]
				|| state[j][size - 1 - j] == 0)
			result &= false;
	}

	return result;

}

int GameState::returnSlot(int x, int y) {
	return state[x][y];
}

bool GameState::isValidMove(int x, int y) {
	if (x < size && y < size)
		return state[x][y] == 0;
	else {
		cout << " Enter a valid position" << endl;
		return false;
	}

}
void GameState::Move(int x, int y, int player) {
	state[x][y] = player;
}

int** GameState::returnState() {
	return state;
}


/*
 * main.cpp
 *
 *  Created on: Aug 23, 2020
 *      Author: nithish
 */


#include<iostream>
#include "Game.h"
#include "utils.h"
#include "state.h"

using namespace std;

int main(int argc, char *argv[]) {
// ./tictactoe game single token1 X size 3
// ./tictactoe game multi token1 X token2 O size 3

	if(!checkArguments(argc, argv))
		  return 0;
	auto parsed = parseInput(argc, argv);

	Game_Engine game_obj(parsed["game"],parsed["token1"], parsed["token2"]);
	game_obj.play();

	return 0;
}



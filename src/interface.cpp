/*
 * interface.cpp
 *
 *  Created on: Aug 24, 2020
 *      Author: nithish
 */


#include "interface.h"

using namespace std;

Interface Interface::interfaceInstance;

std::pair<int,int> Interface::returnPlayerInput(){
	int user_input;
	cin>>user_input;
	// x, y coordinates
	if(user_input<10)
	return make_pair((user_input-1)/grid_size, (user_input-1)%grid_size );
	else
		return make_pair(21,21);
}


void Interface::InitialDisplay(){

	cout << "Position Number for the inputs" << '\n';
	cout << "-------------" << '\n';;
	cout << "| 1 | 2 | 3 |" << '\n';
	cout << "-------------" << '\n';;
	cout << "| 4 | 5 | 6 |" << '\n';
	cout << "-------------" << '\n';;
	cout << "| 7 | 8 | 9 |" << '\n';
}

void Interface::Display(int** _state, string token1, string token2){

	for(int i=0;i<grid_size; i++){
		for(int j=0; j< grid_size; j++){
			if(_state[i][j]== 1 )
				state[i][j] = token1;
			else if(_state[i][j]== 2 )
				state[i][j] = token2;
		}
	}
	cout << "-------------" << '\n';;
	cout << "| "<< state[0][0] << "| " << state[0][1] << " | "<< state[0][2] <<" |" << '\n';
	cout << "-------------" << '\n';;
	cout << "| "<< state[1][0] << "| " << state[1][1] << " | "<< state[1][2] <<" |" << '\n';
	cout << "-------------" << '\n';
	cout << "| "<< state[2][0] << "| " << state[2][1] << " | "<< state[2][2] <<" |" << '\n';
	cout << "-------------" << '\n';
}




Interface::~Interface(){}

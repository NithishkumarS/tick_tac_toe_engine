/*
 * Game.cpp
 *
 *  Created on: Aug 23, 2020
 *      Author: nithish
 */

#include <string>
#include <iostream>
#include <time.h>
#include "Game.h"
#include "factory.h"


using namespace std;

Game_Engine::Game_Engine( string _game_type, string _token1, string _token2, int _size){
	size = _size;
	game_type = _game_type;
	token1 = _token1;
	token2 = _token2;
	agent1 = player::Agents("human");

	if(game_type.compare("single") ==0){
		agent2 = player::Agents("AI");
	}
	else if(game_type.compare("multi") ==0){
		agent2 = player::Agents("human");
	}
	ifc_obj = Interface::getInstance();
	ifc_obj->InitialDisplay();
}


Game_Engine::~Game_Engine(){
	delete agent2;
	delete agent1;
}

int Game_Engine::getPlayer(bool turn){
	return (turn== false) ? 1:2;
}
void Game_Engine::play(){

	bool restart = false;
	bool win = false;
	int move_id = 0;

	srand (time(NULL));
	int val = rand() % 2 + 1;
	bool turn = (val ==1);

	std::pair<int, int> pos;
	board.Initialize();


	while(move_id < 9 && !win){

		if(turn){
			cout<<"player1"<<endl;
			pos = agent1->move(board.returnState());
			turn  = !turn;
		}
		else{
			cout<<"player2"<<endl;
			pos = agent2->move(board.returnState());
			turn  = !turn;
		}

		if(board.isValidMove(pos.first, pos.second)){
			board.Move(pos.first, pos.second, getPlayer(turn) );

			move_id++;
			if (board.isGameOver()){
				win = true;}
		}
		else{
			cout<<" flip"<<endl;
			turn  != turn;
		}
		ifc_obj->Display(board.returnState(), token1, token2);
		if(win){
			cout<<" Winner is player"<< getPlayer(turn) <<endl ;
			return;
		}

	}

	cout<< "Draw"<<endl;
}

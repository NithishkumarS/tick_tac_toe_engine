/*
 * main.cpp
 *
 *  Created on: Aug 24, 2020
 *      Author: nithish
 */

#include <gtest/gtest.h>

int main(int argc, char** argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}




#include <gtest/gtest.h>
#include <iostream>
#include "AI.h"
#include "Game.h"

int** returnState(){
	int** state = new int*[3];
	for (int i = 0; i < 3; i++) {
		state[i] = new int[3];
		for (int j = 0; j < 3; j++)
			state[i][j] = 0;
		}
return state;
}


TEST(AI_class, IO) {
	AI obj;
	EXPECT_EQ(std::make_pair(0,0), obj.move(returnState()));
}



TEST(Game_class, IO) {
	Game_Engine obj("single", "X");
	EXPECT_EQ(2, obj.getPlayer(true));
}


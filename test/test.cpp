/*
 * test.cpp
 *
 * @brief Unit testing the functions
 *
 *  Created on: Aug 24, 2020
 *      Author: nithish
 */


#include <gtest/gtest.h>
#include <vector>
#include "state.h"
#include "interface.h"


using namespace std;
/**
 * @brief test for checking whether the "operation()" function is correct
 */
TEST(Functions, boardclass) {

	GameState board;
	EXPECT_EQ(1, board.isValidMove(1,1));
}

/*
 * @brief Checks the IO of move function
 */
TEST(GameState, IO ) {

	GameState board;
	board.Move(0,0,1);
	EXPECT_EQ(1, board.returnSlot(0,0));
}

/*
 * @brief Tests to check the winning condition
 */

TEST(Gameover, row_check) {

	GameState board;
	board.Move(0,0,1);
	board.Move(0,1,1);
	board.Move(0,2,1);
//	board.Display(board.returnState());

	EXPECT_EQ(1, board.isGameOver());
}

TEST(Gameover, column_check) {

	GameState board2;
	board2.Move(0,0,1);
	board2.Move(1,0,1);
	board2.Move(2,0,1);
//	board2.Display(board2.returnState());
	EXPECT_EQ(1, board2.isGameOver());
}

TEST(Gameover, diagonal_check) {

	GameState board3;
	board3.Move(0,0,1);
	board3.Move(1,1,1);
	board3.Move(2,2,1);
//	board3.Display(board3.returnState());
	EXPECT_EQ(true, board3.isGameOver());
}

TEST(Gameover, anti_diagonal_check) {

	GameState board4;
	board4.Move(0,2,1);
	board4.Move(1,1,1);
	board4.Move(2,0,1);
//	board4.Display(board4.returnState());
	EXPECT_EQ(1, board4.isGameOver());
}


//TEST(InterfaceClass, functions) {
//
//	std::istringstream in("2");
//	Interface* ptr = Interface::getInstance();
//	EXPECT_EQ(make_pair(0,0), ptr->returnPlayerInput());
//}


